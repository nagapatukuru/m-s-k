package hu.jacocowithmaven;

import java.util.function.BinaryOperator;

public final class Calculator {

    private Calculator() {
        throw new IllegalStateException("Utility class");
    }

    private static final BinaryOperator<Integer> addOperator = Integer::sum;
    private static final BinaryOperator<Integer> substractOperator = (a, b) -> a - b;
    private static final BinaryOperator<Integer> multiplyOperator = (a, b) -> a * b;
    private static final BinaryOperator<Integer> divideOperator = (a, b) -> a / b;

    public static int add(int x, int y) {
        return addOperator.apply(x, y);
    }

    public static int substract(int x, int y) {
        return substractOperator.apply(x, y);
    }

    public static int multiply(int x, int y) {
        return multiplyOperator.apply(x, y);
    }

    public static int divide(int x, int y) {
        return divideOperator.apply(x, y);
    }
}
