package hu.jacocowithmaven;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class CalculatorTest {

    @Test
    public void testAdd() {
        Assertions.assertEquals(20, Calculator.add(5, 15));
    }

    @Test
    public void testSubstract() {
        Assertions.assertEquals(-10, Calculator.substract(5, 15));
    }

    @Test
    public void testMultiply() {
        Assertions.assertEquals(75, Calculator.multiply(5, 15));
    }

    @Test
    public void testDivide() {
        Assertions.assertEquals(0, Calculator.divide(5, 15));
    }
}
